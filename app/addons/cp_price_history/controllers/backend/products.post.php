<?php
/*****************************************************************************
*                                                        © 2019 Cart-Power   *
*           __   ______           __        ____                             *
*          / /  / ____/___ ______/ /_      / __ \____ _      _____  _____    *
*      __ / /  / /   / __ `/ ___/ __/_____/ /_/ / __ \ | /| / / _ \/ ___/    *
*     / // /  / /___/ /_/ / /  / /_/_____/ ____/ /_/ / |/ |/ /  __/ /        *
*    /_//_/   \____/\__,_/_/   \__/     /_/    \____/|__/|__/\___/_/         *
*                                                                            *
*                                                                            *
* -------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license *
* and  accept to the terms of the License Agreement can install and use this *
* program.                                                                   *
* -------------------------------------------------------------------------- *
* website: https://store.cart-power.com                                      *
* email:   sales@cart-power.com                                              *
******************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    return;
}

if ($mode == 'update'&& Registry::get('addons.cp_price_history.cp_price_history_for_products') == 'Y') {
    
    Registry::set('navigation.tabs.cp_price_history', array (
        'title' => __('cp_price_history'),
        'js' => true
    ));    

    $params = $_REQUEST;

    list($logs, $search) = fn_cp_price_history_get_logs($params, DESCR_SL);

    Tygh::$app['view']
        ->assign('logs', $logs)
        ->assign('search', $search);
}


