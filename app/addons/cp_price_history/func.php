<?php
/*****************************************************************************
*                                                        © 2019 Cart-Power   *
*           __   ______           __        ____                             *
*          / /  / ____/___ ______/ /_      / __ \____ _      _____  _____    *
*      __ / /  / /   / __ `/ ___/ __/_____/ /_/ / __ \ | /| / / _ \/ ___/    *
*     / // /  / /___/ /_/ / /  / /_/_____/ ____/ /_/ / |/ |/ /  __/ /        *
*    /_//_/   \____/\__,_/_/   \__/     /_/    \____/|__/|__/\___/_/         *
*                                                                            *
*                                                                            *
* -------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license *
* and  accept to the terms of the License Agreement can install and use this *
* program.                                                                   *
* -------------------------------------------------------------------------- *
* website: https://store.cart-power.com                                      *
* email:   sales@cart-power.com                                              *
******************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_cp_price_history_add_item($data)
{
    if (!empty($data))
    {
        $default_data = array(
            'timestamp_log' => TIME,
            'dispatch' => Registry::get('runtime.controller') . '.' . Registry::get('runtime.mode'),
            'user_id' => Tygh::$app['session']['auth']['user_id']
        );

        $row = array_merge($default_data, $data);
        
        db_query("INSERT INTO ?:cp_product_prices_logs ?e", $row);
    }
}

function fn_cp_price_history_update_product_pre($product_data, $product_id, $lang_code) 
{
    if (!empty($product_id)) 
    { 
        $product_info = db_get_row(
            "SELECT company_id, list_price, price FROM ?:products 
            LEFT JOIN ?:product_prices ON ?:products.product_id = ?:product_prices.product_id
            WHERE ?:products.product_id = ?i AND ?:product_prices.lower_limit = ?i", $product_id, 1); 
    }

    // add list_price log    
    if (!empty($product_data['list_price']) && $product_data['list_price'] != $product_info['list_price']) 
    {
        $data = array (   
            'company_id' => $product_info['company_id'],
            'product_id' => $product_id,
            'price_type' => 'L',
            'price_before' => $product_info['list_price'],
            'price_after' => $product_data['list_price']
        ); 
        
        fn_cp_price_history_add_item($data);
    }

    // add price log 
    if (!empty($product_data['price']) && $product_data['price'] != $product_info['price']) 
    {
        $data = array (
            'company_id' => $product_info['company_id'],
            'product_id' => $product_id,         
            'price_type' => 'P',
            'price_before' => $product_info['price'],
            'price_after' => $product_data['price']
        );

        fn_cp_price_history_add_item($data);
    }

    // add qty_discounts log
    if(!empty($product_data['prices']))
    {     
        foreach ($product_data['prices'] as $prices)
        {    
            $old_price = db_get_field("SELECT price FROM ?:product_prices WHERE product_id = ?i 
                AND lower_limit = ?i", $product_id, $prices['lower_limit'] );

            if ($prices['price'] != $old_price) 
            {          
                if ($prices['price'] != 0.00)
                {
                    $new_price = ($prices['price']);
                    $price = $old_price;

                    $data = array (
                        'company_id' => $product_info['company_id'],
                        'product_id' => $product_id,         
                        'price_type' => 'Q',
                        'price_before' => $price,
                        'price_after' => $new_price
                    );
            
                    fn_cp_price_history_add_item($data);
                }    
            }
        }
    }
}

function fn_cp_price_history_global_update_products($table, $field, $value, $type, $msg, $update_data)
{    
    $company_id = Registry::get('runtime.company_id');
    
    if ($company_id != 0){
        $product_ids = db_get_fields("SELECT product_id FROM ?:products WHERE company_id = '$company_id'");
    } else {
        $product_ids = db_get_fields("SELECT product_id FROM ?:products ");
    }

    foreach ($product_ids as $product_id) 
    {   
        if(empty($product_id)){
            continue;
        }

        $product_data = db_get_row("SELECT product,list_price,percentage_discount,company_id FROM ?:product_descriptions 
        LEFT JOIN ?:products ON ?:product_descriptions.product_id = ?:products.product_id  
        LEFT JOIN ?:product_prices ON ?:product_descriptions.product_id = ?:product_prices.product_id  
        WHERE ?:products.product_id = $product_id");

        //prices
        $old_price = db_get_field("SELECT price FROM ?:product_prices WHERE product_id = ?i", $product_id);
        $old_list_price = db_get_field("SELECT list_price FROM ?:products WHERE product_id = ?i", $product_id);
        
        if(!empty($update_data['price'])) {
            $price = $old_price + $update_data['price']; 
        }

        if(!empty($update_data['list_price'])) {
            $list_price = $old_list_price + $update_data['list_price'];
        }
        
        if($field[0] = 'price' && !empty($price)  && $price != $old_price)
        {
            $data = array (
                'company_id' => $product_data['company_id'],
                'product_id' => $product_id,         
                'price_type' => 'P',
                'price_before' => $old_price,
                'price_after' => $price
            );
    
            fn_cp_price_history_add_item($data);        
        }

        if($field[0] = 'list_price' && !empty($list_price) && $list_price != $old_list_price)
        {  
            $data = array (
                'company_id' => $product_data['company_id'],
                'product_id' => $product_id,         
                'price_type' => 'L',
                'price_before' => $old_list_price,
                'price_after' => $list_price
            );
    
            fn_cp_price_history_add_item($data);      
        }        
    }
} 

function fn_cp_price_history_get_logs($params = array(), $lang_code = CART_LANGUAGE) 
{
    $dispatch = Registry::get('runtime.controller').'.'.Registry::get('runtime.mode');

    $params = array_merge(array(
        'items_per_page' => 0,
        'page' => 1,
    ), $params);

    $sortings = array (
        'date' => 'timestamp_log',
        'update_channel' => 'dispatch',
        'who_update' => 'user_id'
    );

    $condition = array();

    if (isset($params['name']) && fn_string_not_empty($params['name'])) {
        $params['name'] = trim($params['name']);
        $condition[] = db_quote("?:product_descriptions.product LIKE ?l", '%' . $params['name'] . '%');
    }

    if (!empty($params['period'])) {
        list($params['time_from'], $params['time_to']) = fn_create_periods($params);
        $condition [] = db_quote("timestamp_log >= ?i AND timestamp_log <= ?i", $params['time_from'], $params['time_to']);
    }

    if (!empty($params['price_type'])) {
        $condition[] = db_quote("?:cp_product_prices_logs.price_type = ?s", $params['price_type']);
    }

    if (isset($params['price_before']) && fn_string_not_empty($params['price_before'])) {
        $params['price_before'] = trim($params['price_before']);
        $condition[] = db_quote("price_before LIKE ?l", '%' . $params['price_before'] . '%');
    }

    if (isset($params['price_after']) && fn_string_not_empty($params['price_after'])) {
        $params['price_after'] = trim($params['price_after']);
        $condition[] = db_quote("price_after LIKE ?l", '%' . $params['price_after'] . '%');
    }

    if (isset($params['who_update']) && fn_string_not_empty($params['who_update'])) {
        $params['who_update'] = trim($params['who_update']);
        $condition[] = db_quote("?:users.firstname OR ?:users.lastname LIKE ?l", '%' . $params['who_update'] . '%');
    }

    if (!empty($params['update_type'])) {
        $condition[] = db_quote("?:cp_product_prices_logs.dispatch = ?s", $params['update_type']);
    }

    if ($dispatch = 'cp_price_history.manage') {
        $company_id = Registry::get('runtime.company_id');
        if ($company_id != 0){
            $condition[] = db_quote("?:cp_product_prices_logs.company_id = ?i", $company_id);
        }
    }

    if (!empty($params['product_id'])) {
        $condition[] = db_quote("?:cp_product_prices_logs.product_id = ?i", $params['product_id']);
    } 
    
    $condition[] = db_quote("?:product_descriptions.lang_code = ?s", $lang_code);

    $condition_str = $condition ? (' WHERE ' . implode(' AND ', $condition)) : '';

    $sorting_str = db_sort($params, $sortings, 'date', 'desc');

    $join_str =  db_quote(" 
        LEFT JOIN ?:product_descriptions ON ?:cp_product_prices_logs.product_id = ?:product_descriptions.product_id
        LEFT JOIN ?:users ON ?:cp_product_prices_logs.user_id = ?:users.user_id");

    $limit = '';
    if (empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(prices_log_id) FROM ?:cp_product_prices_logs". $join_str . $condition_str);
        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
    }

    $fields = array(
        '?:cp_product_prices_logs.*',
        '?:product_descriptions.product',
        '?:users.firstname',
        '?:users.lastname' 
    );

    $fields_str = implode(', ', $fields);
    
    $items = db_get_array(
        "SELECT " 
        . $fields_str 
        . " FROM ?:cp_product_prices_logs" 
        . $join_str 
        . $condition_str 
        . $sorting_str 
        . $limit
    );

    return array($items, $params);
}

function fn_cp_price_history_clean_logs()
{ 
    $company_id = Registry::get('runtime.company_id');

    if (empty($company_id)){
        db_query("TRUNCATE TABLE ?:cp_product_prices_logs");
    } else {
        db_query("DELETE FROM ?:cp_product_prices_logs WHERE company_id = ?i", $company_id);
    }
}


