<?php
/*****************************************************************************
*                                                        © 2019 Cart-Power   *
*           __   ______           __        ____                             *
*          / /  / ____/___ ______/ /_      / __ \____ _      _____  _____    *
*      __ / /  / /   / __ `/ ___/ __/_____/ /_/ / __ \ | /| / / _ \/ ___/    *
*     / // /  / /___/ /_/ / /  / /_/_____/ ____/ /_/ / |/ |/ /  __/ /        *
*    /_//_/   \____/\__,_/_/   \__/     /_/    \____/|__/|__/\___/_/         *
*                                                                            *
*                                                                            *
* -------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license *
* and  accept to the terms of the License Agreement can install and use this *
* program.                                                                   *
* -------------------------------------------------------------------------- *
* website: https://store.cart-power.com                                      *
* email:   sales@cart-power.com                                              *
******************************************************************************/

function fn_cp_price_history_exim_price_logs($import_data) 
{
    foreach ($import_data as $_import_data) 
    {   
        if (empty($_import_data['product_code'])) {
            continue;
        }

        $product_info = db_get_row("SELECT product_id, company_id, list_price FROM ?:products WHERE ?:products.product_code = ?s", $_import_data['product_code']);
        
        $price = db_get_field("SELECT price FROM ?:product_prices WHERE product_id = ?i AND lower_limit = ?i", $product_info['product_id'], 1); 
        
        if ($_import_data['list_price'] != $product_info['list_price'])
        {    
            $data = array (
                'company_id' => $product_info['company_id'],
                'product_id' => $product_info['product_id'],           
                'price_type' => 'L',
                'price_before' => $product_info['list_price'],
                'price_after' => $_import_data['list_price']
            );
    
            fn_cp_price_history_add_item($data); 
        }
        
        if ($_import_data['price'] != $price)
        {    
            $data = array (
                'company_id' => $product_info['company_id'],
                'product_id' => $product_info['product_id'],        
                'price_type' => 'P',
                'price_before' => $price,
                'price_after' => $_import_data['price']
            );
    
            fn_cp_price_history_add_item($data); 
        }   
    }
} 