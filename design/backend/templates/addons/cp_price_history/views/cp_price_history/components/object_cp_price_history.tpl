<div class="hidden" id="content_cp_price_history">

    <form action="{""|fn_url}" method="post" name="manage_cp_price_history_form" class="form-horizontal form-edit cm-ajax">
    <input type="hidden" name="result_ids" value="pagination_contents,tools_cp_price_history_buttons" />

    {include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}

    {assign var="return_url" value=$config.current_url|escape:"url"}
    {assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}

    {assign var="rev" value=$smarty.request.content_id|default:"pagination_contents"}
    {assign var="c_icon" value="<i class=\"icon-`$search.sort_order_rev`\"></i>"}
    {assign var="c_dummy" value="<i class=\"icon-dummy\"></i>"}

    {if $logs}
        <div class="table-wrapper-responsive">
            <table width="100%" class="table table-middle table-responsive">
                <thead>
                    <tr>
                        <th>
                            <a class="cm-ajax" href="{"`$c_url`&sort_by=date&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("date")}{if $search.sort_by == "date"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a>
                        </th>
                        <th>{__("cp_price_type")}</th>
                        <th>{__("cp_price_before")}</th>
                        <th>{__("cp_price_after")}</th>
                        <th>
                            <a class="cm-ajax" href="{"`$c_url`&sort_by=who_update&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("cp_who_update")}{if $search.sort_by == "who_update"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}
                        </th>
                        <th>
                            <a class="cm-ajax" href="{"`$c_url`&sort_by=update_channel&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("cp_update_channel")}{if $search.sort_by == "update_channel"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}
                        </th>    
                    </tr>
                </thead>
                <tbody>
                {foreach from=$logs item=log}
                    <tr class="cm-row">
                        <td class="nowrap" data-th="{__("date")}">{$log.timestamp_log|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}</td>
                        <td data-th="{__("cp_price_type")}">
                            {if $log.price_type == 'P'}{__("cp_price_of_good")}{/if}
                            {if $log.price_type == 'L'}{__("list_price")}{/if}
                            {if $log.price_type == 'Q'}{__("cp_wholesale_price")}{/if}
                        </td>

                        <td>{$log.price_before}</td>
                        <td>{$log.price_after}</td>
                        <td>
                            <a href="{"profiles.update&user_id={$log.user_id}&user_type=A"|fn_url}">{$log.firstname} {$log.lastname}</a>
                        </td>
                        <td>
                            {if $log.dispatch == 'exim.import'}{__("import")}{/if}
                            {if $log.dispatch == 'products.update'}{__("cp_direct_update")}{/if}
                            {if $log.dispatch == 'products.m_update'}{__("cp_massive_update")}{/if}
                            {if $log.dispatch == 'products.m_update_prices'}{__("cp_list_update")}{/if}
                            {if $log.dispatch == 'products.global_update'}{__("cp_global_update")}{/if}
                        </td>
                    </tr>
                </tbody>
            {/foreach}
            </table>
        </div>
    {else}
        <p class="no-items">{__("no_data")}</p>
    {/if}

    <div class="clearfix">
        {include file="common/pagination.tpl" div_id=$smarty.request.content_id}
    </div>

    </form>

</div>


