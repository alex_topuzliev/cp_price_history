{if $in_popup}
    <div class="adv-search">
    <div class="group">
{else}
    <div class="sidebar-row">
    <h6>{__("search")}</h6>
{/if}
<form name="search_form" action="{""|fn_url}" method="get" class="{$form_meta}">

{if $smarty.request.redirect_url}
<input type="hidden" name="redirect_url" value="{$smarty.request.redirect_url}" />
{/if}

{if $selected_section != ""}
<input type="hidden" id="selected_section" name="selected_section" value="{$selected_section}" />
{/if}

{if $search.user_type}
<input type="hidden" name="user_type" value="{$search.user_type}" />
{/if}

{if $put_request_vars}
    {array_to_fields data=$smarty.request skip=["cp_price_history"] escape=["data_id"]}
{/if}

{capture name="simple_search"}
{$extra nofilter}

<div class="sidebar-field">
    <label for="elm_name">{__("name")}</label>
    <div class="break">
        <input type="text" name="name" id="elm_name" value="{$search.name}" />
    </div>
</div>

<div class="control-group">
    <label for="price_type" class="control-label">{__("cp_price_type")}</label>
    <div class="controls">
        <select name="price_type" id="price_type">
            <option value="">--</option>
            <option value="P" {if $search.price_type == 'P'}selected{/if}>{__("cp_price_of_good")}</option>
            <option value="L" {if $search.price_type == 'L'}selected{/if}>{__("list_price")}</option>
            <option value="Q" {if $search.price_type == 'Q'}selected{/if}>{__("cp_wholesale_price")}</option>
        </select>
    </div>
</div>

<div class="sidebar-field">
    <label for="elm_price_before">{__("cp_price_before")}</label>
    <div class="break">
        <input type="text" name="price_before" id="elm_price_before" value="{$search.price_before}" />
    </div>
</div>

<div class="sidebar-field">
    <label for="elm_price_after">{__("cp_price_after")}</label>
    <div class="break">
        <input type="text" name="price_after" id="elm_price_after" value="{$search.price_after}" />
    </div>
</div>

<div class="sidebar-field">
    <label for="elm_who_update">{__("cp_who_update")}</label>
    <div class="break">
        <input type="text" name="who_update" id="elm_who_update" value="{$search.who_update}" />
    </div>
</div>

<div class="control-group">
    <label for="update_type" class="control-label">{__("cp_update_channel")}</label>
    <div class="controls">
        <select name="update_type" id="update_type">
            <option value="">--</option>
            <option value="exim.import" {if $search.update_type == 'exim.import'}selected{/if}>{__("import")}</option>
            <option value="products.update" {if $search.update_type == 'products.update'}selected{/if}>{__("cp_direct_update")}</option>
            <option value="products.m_update" {if $search.update_type == 'products.m_update'}selected{/if}>{__("cp_massive_update")}</option>
            <option value="products.m_update_prices" {if $search.update_type == 'products.m_update_prices'}selected{/if}>{__("cp_list_update")}</option>
            <option value="products.global_update" {if $search.update_type == 'products.global_update'}selected{/if}>{__("cp_global_update")}</option>
        </select>
    </div>
</div>

{include file="common/period_selector.tpl" period=$period display="form"}

{/capture}

{$no_adv_link = "ULTIMATE"|fn_allowed_for}
{include file="common/advanced_search.tpl" simple_search=$smarty.capture.simple_search advanced_search=$smarty.capture.advanced_search dispatch=$dispatch view_type="users" in_popup=$in_popup}

</form>

{if $in_popup}
</div></div>
{else}
</div><hr>
{/if}
